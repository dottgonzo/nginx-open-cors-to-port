#!/bin/bash

VERSION=$1
REGISTRY_URI="registry.gitlab.com/dottgonzo/nginx-open-cors-to-port"

if [ -z "$VERSION" ]; then
echo building $REGISTRY_URI with version: \"master\"
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t $REGISTRY_URI:master --push .

else
echo building $REGISTRY_URI with version: \"master\" and \"$VERSION\"
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t $REGISTRY_URI:master -t $REGISTRY_URI:$VERSION --push .

fi


