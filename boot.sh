#! /bin/sh

if [ -z "$HOST" ]; then
    echo "HOST is not set. Using default value of 127.0.0.1"
    HOST="127.0.0.1"
fi

if [ -z "$PORT" ]; then
    echo "PORT is not set. Please set the PORT environment variable"
    exit 1
fi

if [ -z "$PROTOCOL" ]; then
    echo "PROTOCOL is not set. Using default value of http"
    PROTOCOL="http"
fi

if [ -z "$NGINX_PORT" ]; then
    echo "NGINX_PORT is not set. Using default value of 80"
    NGINX_PORT="80"
fi

sed -i "s|proxy_pass XXXX://YYYYYYYYY:ZZZZZ;|proxy_pass ${PROTOCOL}://${HOST}:${PORT};|g" /etc/nginx/nginx.conf
sed -i "s|listen 80;|listen ${NGINX_PORT};|g" /etc/nginx/nginx.conf

echo "Proxying to ${PROTOCOL}://${HOST}:${PORT}"
# exit 0
nginx -g 'daemon off;';






