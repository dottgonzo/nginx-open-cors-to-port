FROM nginx:alpine
CMD ["/boot.sh"]
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./boot.sh /boot.sh
